package com.StepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.Runner.BaseClass;

import cucumber.api.java.en.Then;

public class SearchResultsPageStepDef extends BaseClass {

	@Then("^User should see the search product\\.$")
	public void user_should_see_the_search_product() throws Throwable {
		searchResultsPage.verifySearchResultsPage();
	}
	
	@Then("^User should see an error message$")
	public void user_should_see_an_error_message() throws Throwable {
		searchResultsPage.verifySearchResultError();
	}
	
	@Then("^User should be prompt to fill the search field$")
	public void user_should_be_prompt_to_fill_the_search_field() throws Throwable {
		searchResultsPage.completeProductSearch();
	}
	
	@Then("^User should get an error message$")
	public void user_should_get_an_error_message() throws Throwable {
		searchResultsPage.verifyInvalidProductCode();
	}
	
	@Then("^User should see search products$")
	public void user_should_see_search_products() throws Throwable {
		searchResultsPage.verifyValidProductCode();
	}

	@Then("^User should see the selected colour with products$")
	public void user_should_see_the_selected_colour_with_products() throws Throwable {
		searchResultsPage.verifyProductWithColour();
		//Assert.assertTrue(driver.findElement(By.cssSelector("#js-plp-search header h1")).getText().toLowerCase().contains("black"));
	}

	@Then("^User should see related products$")
	public void user_should_see_related_products() throws Throwable {
		searchResultsPage.verifyRelatedProducts();
	}
	
	@Then("^User should get the details of nearest stores and its opening times$")
	public void user_should_get_the_details_of_nearest_stores_and_its_opening_times() throws Throwable {
		searchResultsPage.verifyPostcode();
	}
	@Then("^User should get an not found error message$")
	public void user_should_get_an_not_found_error_message() throws Throwable {
		searchResultsPage.verifyInvalidPostcode();
	}
	
	@Then("^Product should add to basket$")
	public void product_should_add_to_basket() throws Throwable {
		searchResultsPage.verifyProductDetailResults();
}
	@Then("^Product should remove from the basket$")
	public void product_should_remove_from_the_basket() throws Throwable {
		searchResultsPage.verifyRemovalProductResult();
	}
	
	@Then("^User should direct to homepage$")
	public void user_should_direct_to_homepage() throws Throwable {
		searchResultsPage.verifyValidLoginResults();
	}

	@Then("^User shouldn't direct to homepage\\.$")
	public void user_shouldn_t_direct_to_homepage() throws Throwable {
		searchResultsPage.verifyInvalidLoginResults();
	}
	
	@Then("^User should see welcome message with name$")
	public void user_should_see_welcome_message_with_name() throws Throwable {
		searchResultsPage.verifyLoginHomePage();
	}
	
	@Then("^User should get error message$")
	public void user_should_get_error_message() throws Throwable {
		searchResultsPage.verifyLoginErrorMessage();
	}
	
	@Then("^User should get payment confirmation$")
	public void user_should_get_payment_confirmation() throws Throwable {
		searchResultsPage.verifyPaymentConfirmation();
	}
	
	@Then("^Click on click and collect button$")
	public void click_on_click_and_collect_button() throws Throwable {
		searchResultsPage.verifyClickAndCollectResults();
	}
}
