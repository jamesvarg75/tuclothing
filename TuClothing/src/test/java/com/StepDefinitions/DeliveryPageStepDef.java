package com.StepDefinitions;

import com.Runner.BaseClass;

import cucumber.api.java.en.Given;

public class DeliveryPageStepDef extends BaseClass{
	
	@Given("^User selects home delivery option and then continue$")
	public void user_selects_home_delivery_option_and_then_continue() throws Throwable {
		deliveryPage.verifyHomeDeliveryDetails();
	}

	@Given("^User enters the mandatory fields and find address$")
	public void user_enters_the_mandatory_fields_and_find_address() throws Throwable {
		deliveryPage.verifyDeliveryAddress();
	}
}
