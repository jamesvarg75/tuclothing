package com.StepDefinitions;

import com.Runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class ProductDetailsPageStepDef extends BaseClass {

	@Given("^User is on product details page$")
	public void user_is_on_product_details_page() throws Throwable {
		productDetailsPage.verifyProductDeailsPage();
	}
	
	@Given("^User select a product and product size and quantity$")
	public void user_select_a_product_and_product_size_and_quantity() throws Throwable {
		productDetailsPage.verifyCheckoutProducts();
	}
	
}
