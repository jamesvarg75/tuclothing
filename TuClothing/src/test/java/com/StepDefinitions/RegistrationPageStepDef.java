package com.StepDefinitions;

import java.util.Map;

import com.Runner.BaseClass;

import cucumber.api.DataTable;
import cucumber.api.java.en.When;

public class RegistrationPageStepDef extends BaseClass{

//	@When("^User enter valid credentials$")
//	public void user_enter_valid_credentials() throws Throwable {
//		registrationPage.verifyValidLoginCredentials();
//	}
	
	@When("^User enter valid credentials$")
	public void user_enter_valid_credentials(DataTable userDetails) throws Throwable {
		Map<String, String> loginDetails = userDetails.asMap(String.class, String.class);
		String emailValue = loginDetails.get("email");
		String passwordValue = loginDetails.get("password");
		registrationPage.verifyValidLoginCredentials(emailValue,passwordValue);
	}

	@When("^User enter login button$")
	public void user_enter_login_button() throws Throwable {
		registrationPage.verifyLoginButton();
	}

	@When("^User enter valid userid and invalid password$")
	public void user_enter_valid_userid_and_invalid_password() throws Throwable {
		registrationPage.verifyLoginWithInvalidPassword();	
	}
	
	@When("^User enter invalid userid and valid password$")
	public void user_enter_invalid_userid_and_valid_password() throws Throwable {
		registrationPage.verifyLoginWithInvalidUserid();
	}
}
