package com.StepDefinitions;

import com.Runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class PaymentPageStepDef extends BaseClass{

	@Given("^User selects click & collect option and then continue$")
	public void user_selects_click_collect_option_and_then_continue() throws Throwable {
		paymentPage.verifyClickAndCollectPayment();	
	}
	
	@Given("^User clicks on proceed to payment button$")
	public void user_clicks_on_proceed_to_payment_button() throws Throwable {
		paymentPage.verifyProceedPayment();
	}
	
	@Given("^User enters the card details on the payment page$")
	public void user_enters_the_card_details_on_the_payment_page() throws Throwable {
		paymentPage.verifyUserCardDetails();
	}
	
	@When("^User clicks on payment button$")
	public void user_clicks_on_payment_button() throws Throwable {
		paymentPage.verifyPayment();
	}
	
	@Given("^User selects the type of home delivery and proceed$")
	public void user_selects_the_type_of_home_delivery_and_proceed() throws Throwable {
		paymentPage.verifyHomeDeliveryPaymentDetails();
	}
	
}
