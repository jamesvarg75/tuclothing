package com.StepDefinitions;

import org.openqa.selenium.chrome.ChromeDriver;

import com.Runner.BaseClass;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends BaseClass{
	
	private static String baseURL = "https://tuclothing.sainsburys.co.uk/";
	@Before
	public void start() {		
		System.setProperty("webdriver.chrome.driver", "./src/test/resources/Drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get(baseURL);
		driver.manage().window().maximize();
	}
	@After
	
	public void close() {
		driver.close();
	}
	
}
