//package com.StepDefinitions;
//
//import org.junit.Assert;
//import org.openqa.selenium.By;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.support.ui.Select;
//
//import cucumber.api.java.After;
//import cucumber.api.java.en.Given;
//import cucumber.api.java.en.Then;
//import cucumber.api.java.en.When;
//
//
//public class AllTestDefinitions {
//	public static WebDriver driver;
//	private static String baseURL = "https://tuclothing.sainsburys.co.uk/";
//	private static By TEXTSEARCH = By.cssSelector("#search");
//	private static By TEXTSEARCHBUTTON= By.cssSelector(".searchButton");
//	private static By MENCATEGORYBUTTON = By.linkText("Men");
//	private static By MENCATEGORYSUBBUTTON = By.linkText("Jeans");
//	private static By MENPRODUCTLIST = By.cssSelector(".image-component img");
//	private static By SIZEDROPDOWN = By.cssSelector("#select-size");
//	private static By QNTYDROPDOWN = By.cssSelector("#productVariantQty");
//	private static By ADDTOCART = By.cssSelector("#AddToCart");
//	private static By CARTBUTTON = By.cssSelector("#cart_button");
//	private static By NEWQNTYDROPDOWN = By.cssSelector("#quantity0");
//	private static By UPDATEQNTY= By.cssSelector("#QuantityProduct_0");
//	private static By REMOVEPRODUCT= By.cssSelector("#RemoveProduct_1");
//	private static By RLOGIN= By.linkText("Tu Log In / Register");
//	private static By RREGISTER = By.cssSelector(".regToggle");
//	private static By RSUBMIT= By.cssSelector("#submit-register");
//	private static By REMAIL= By.cssSelector("#register_email");
//	private static By RTITLE = By.cssSelector("#register_title");
//	private static By RFIRSTNAME = By.cssSelector("#register_firstName");
//	private static By RLASTNAME= By.cssSelector("#register_lastName");
//	private static By RRPASSWORD= By.cssSelector("#password");
//	private static By RUSERNAME = By.cssSelector("#j_username");
//	private static By RPASSWORD = By.cssSelector("#j_password");
//	private static By SUBMITLOGIN = By.cssSelector("#submit-login");
//	private static By LOUNGEWEAR = By.linkText("Loungewear");
//	private static By LOUNGECOLLECTIONS = By.cssSelector(".image-component img");
//	private static By ADDBASKET = By.cssSelector("#basket-title");
//	private static By ADDTOCHECKOUT= By.cssSelector("a[class='ln-c-button ln-c-button--primary']");
//	private static By PROCEEDTOCHECKOUT = By.cssSelector("a[class='ln-c-button ln-c-button--primary ln-u-soft-sides*1/4 tuButton']");
//	private static By GEMAIL= By.cssSelector("#guest_email");
//	private static By GUESTCHECKOUT = By.cssSelector("button[data-testid='guest_checkout']");
//	private static By CLICKACOLLECT = By.cssSelector("label[for='CLICK_AND_COLLECT']");
//	private static By GUESTSUBMIT= By.cssSelector("button[data-testid='submit-button']");
//	private static By CANDCCONTINUE = By.cssSelector("input[data-testid='continue']");
//	private static By CANDCADDRESS = By.cssSelector("#lookup");
//	private static By ADDRESSLOOKUP = By.cssSelector("button[data-testid='lookup-submit']");
//	private static By SUBMITFORCARDDETAILS = By.cssSelector("button[class='StoreCard-module__selectStoreButton--3CFno']");
//	private static By HDELIVERYPAYMENT = By.cssSelector("button[data-testid='continueToPayment']");
//	private static By HCARDPAYMENT = By.linkText("Pay with a card");
//	private static By NEWTITLE = By.cssSelector("#newTitle");
//	private static By FIRSTNAME = By.cssSelector("input[data-testid='firstName']");
//	private static By SURNAME = By.cssSelector("input[data-testid='surname']");
//	private static By HOUSENO = By.cssSelector("input[data-testid='houseNumber']");
//	private static By POSTCODE = By.cssSelector("input[data-testid='postCode']");
//	private static By FINDADDRESS = By.cssSelector("button[data-testid='findAddress']");
//	private static By TERMSANDCOND = By.cssSelector("label[data-testid='termsAndConditions']");
//	private static By CONTPAYMENT = By.cssSelector("button[id='contPayment']");
//	private static By HOMEDELIVERY = By.cssSelector("label[for='HOME_DELIVERY']");
//	private static By CLICKHOMEHOMEDELIVERY= By.cssSelector("input[data-testid='continue']");
//	private static By HTITLE= By.cssSelector("select[id='address.title']");
//	private static By HFIRSTNAME= By.cssSelector("input[id='address.firstName']");
//	private static By HSURNAME= By.cssSelector("input[id='address.surname']");
//	private static By HHOUSENUMBER= By.cssSelector("input[id='addressDeliveryhouseNameOrNumber']");
//	private static By HPOSTCODE= By.cssSelector("input[id='addressPostcode']");
//	private static By HSEARCHADDRESS= By.cssSelector("button[class='ln-c-button ln-c-button--primary address-lookup ln-u-push-bottom']");
//	private static By HBILLINGADDRESS= By.cssSelector("label[for='useDeliveryAddressForBillingAddress']");
//	private static By HCONTINUE= By.cssSelector("#continue");
//	private static By HSTANDDELIVERY= By.cssSelector("label[for='standard-delivery']");
//	private static By HSTANDDELIVERYCONTINUE = By.cssSelector("input[value='Continue']");
////	private static By HDELIVERYPAYMENT = By.cssSelector("button[data-testid='continueToPayment']");
////	private static By HCARDPAYMENT = By.linkText("Pay with a card");
//	private static By HFINDADDRESS= By.cssSelector("button[data-testid='findAddress']");
//	private static By STORELOCATOR = By.linkText("Tu Store Locator");
//	private static By TEXTSTORELOCATOR= By.cssSelector(".ln-c-text-input.ln-u-push-bottom");
//	private static By STORESEARCH= By.cssSelector("button[class='ln-c-button ln-c-button--primary']");
//	private static By CHECKOUTBUTTON = By.cssSelector(".doCheckoutBut.tuButton.ln-c-button.ln-c-button--primary");
//	private static By SHOWPROMO = By.cssSelector("#showPromo span");
//	private static By CLICKANDCOLLECT = By.linkText("Click & Collect");
//	
//	@Given("^User is on homepage$")
//	public void user_is_on_homepage() throws Throwable {
//		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\chromedriver.exe");
//		driver = new ChromeDriver();
//		driver.get(baseURL);
//		driver.manage().window().maximize();
//		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());
//	}
//
//	@When("^User search for a valid product$")
//	public void user_search_for_a_valid_product() throws Throwable {
//		driver.findElement(TEXTSEARCH).clear();
//		Thread.sleep(3000);
//		driver.findElement(TEXTSEARCH).sendKeys("Tops");
//		driver.findElement(TEXTSEARCHBUTTON).click();
//		//Assert.assertEquals("Search results for 'Tops'", driver.findElement(By.cssSelector("h1")).getText());
//		
//	}
//
//	@Then("^User should see the search product\\.$")
//	public void user_should_see_the_search_product() throws Throwable {
//		Assert.assertEquals("Search results for: Tops | Tu clothing", driver.getTitle());
//	}
//	@When("^User search for an invalid product$")
//	public void user_search_for_an_invalid_product() throws Throwable {
//		driver.findElement(TEXTSEARCH).clear();
//		Thread.sleep(3000);
//		driver.findElement(TEXTSEARCH).sendKeys("Liquor");
//		driver.findElement(TEXTSEARCHBUTTON).click();
//	}
//
//	
//	
//
//	@Then("^User should be prompt to fill the search field$")
//	public void user_should_be_prompt_to_fill_the_search_field() throws Throwable {
//		Thread.sleep(3000);
//		Assert.assertEquals("Please complete a product search", driver.findElement(By.cssSelector("#search-empty-errors")).getText());
//	}
//
//	@When("^User search with invalid productcode$")
//	public void user_search_with_invalid_productcode() throws Throwable {
//		driver.findElement(TEXTSEARCH).clear();
//		Thread.sleep(3000);
//		driver.findElement(TEXTSEARCH).sendKeys("433545335");
//		driver.findElement(TEXTSEARCHBUTTON).click();
//	
//	}
//
//	@Then("^User should get an error message$")
//	public void user_should_get_an_error_message() throws Throwable {
//		Assert.assertEquals("Search results for: 433545335 | Tu clothing", driver.getTitle());
//	}
//	@When("^User search with valid productcode$")
//	public void user_search_with_valid_productcode() throws Throwable {
//		driver.findElement(TEXTSEARCH).clear();
//		Thread.sleep(3000);
//		driver.findElement(TEXTSEARCH).sendKeys("137431127");
//		driver.findElement(TEXTSEARCHBUTTON).click();
//	}
//
//	@Then("^User should see search products$")
//	public void user_should_see_search_products() throws Throwable {
//		Assert.assertEquals("Search results for: 137431127 | Tu clothing", driver.getTitle()); 
//	}
//	@When("^User search product with colour$")
//	public void user_search_product_with_colour() throws Throwable {
//		driver.findElement(TEXTSEARCH).clear();
//		Thread.sleep(3000);
//		driver.findElement(TEXTSEARCH).sendKeys("Black");
//		driver.findElement(TEXTSEARCHBUTTON).click();
//		
//	}
//
//	@Then("^User should see the selected colour with products$")
//	public void user_should_see_the_selected_colour_with_products() throws Throwable {
//		
//		//Assert.assertTrue(driver.findElement(By.cssSelector("#js-plp-search header h1")).getText().toLowerCase().contains("black"));
//		Assert.assertTrue(driver.findElement(By.cssSelector("h1")).getText().toLowerCase().contains("black"));
//	}
//	@When("^User search product with description$")
//	public void user_search_product_with_description() throws Throwable {
//		driver.findElement(TEXTSEARCH).clear();
//		Thread.sleep(3000);
//		driver.findElement(TEXTSEARCH).sendKeys("Yellow Oversized Knitted Top");
//		driver.findElement(TEXTSEARCHBUTTON).click();
//		
//	}
//
//	@Then("^User should see related products$")
//	public void user_should_see_related_products() throws Throwable {
//		Assert.assertTrue(driver.findElement(By.cssSelector("h1")).getText().toLowerCase().contains("yellow oversized knitted top"));
//	}
//
//	@Given("^User is on product details page$")
//	public void user_is_on_product_details_page() throws Throwable {
//		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\chromedriver.exe");
//		driver = new ChromeDriver();
//		driver.get(baseURL);
//		driver.manage().window().maximize();
//		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());
//		driver.findElement(MENCATEGORYBUTTON).click();
//		//Assert.assertEquals("Menswear", driver.findElement(By.cssSelector("h2:contains('Menswear')")).getText());
//		Assert.assertEquals("Mens Clothing | Jeans, T-Shirts, Shirts | Tu Clothing", driver.getTitle());
//		Thread.sleep(2000);
//		driver.findElement(MENCATEGORYSUBBUTTON).click();
//		Thread.sleep(2000);
//		driver.findElements(MENPRODUCTLIST).get(0).click();
//		Thread.sleep(3000);
//	}
//
//	@Given("^User select size and multiple quantity$")
//	public void user_select_size_and_multiple_quantity() throws Throwable {
//		Select sizeDropdown = new Select(driver.findElement(SIZEDROPDOWN));
//		sizeDropdown.selectByIndex(15);
//		Thread.sleep(3000);
//		Select qntyDropdown = new Select(driver.findElement(QNTYDROPDOWN));
//		qntyDropdown.selectByValue("1");
//		Thread.sleep(3000);
//		driver.findElement(ADDTOCART).click();
//		Thread.sleep(3000);
//		qntyDropdown.selectByValue("2");
//		Thread.sleep(3000);
//	}
//
//	@When("^User click on to add basket$")
//	public void user_click_on_to_add_basket() throws Throwable {
//		driver.findElement(ADDTOCART).click(); // no actions will happen, you have to do one one addtocart click.
//		Thread.sleep(3000);
//		driver.findElement(ADDTOCART).click(); // Error message will appear now.
//	}
//
//	@Then("^Product should add to basket$")
//	public void product_should_add_to_basket() throws Throwable {
//		Assert.assertEquals("Qty:2", driver.findElement(By.cssSelector(".prod_quantity")).getText());
//	}
//	@Given("^User select size and quantity and click on to addbasket$")
//	public void user_select_size_and_quantity_and_click_on_to_addbasket() throws Throwable {
//		Select sizeDropdown = new Select(driver.findElement(SIZEDROPDOWN));
//		sizeDropdown.selectByIndex(15);
//		Thread.sleep(3000);
//		Select qntyDropdown = new Select(driver.findElement(QNTYDROPDOWN));
//		qntyDropdown.selectByValue("2");
//		Thread.sleep(3000);				
//		driver.findElement(ADDTOCART).click();
//		Thread.sleep(3000);//			
//		driver.findElement(CARTBUTTON).click();
//		Thread.sleep(3000);//
//		Select newQntyDropdown = new Select(driver.findElement(NEWQNTYDROPDOWN));
//		newQntyDropdown.selectByValue("10");
//		Thread.sleep(3000);
//	}
//
//	@When("^User select remove product option$")
//	public void user_select_remove_product_option() throws Throwable {
//
//		driver.findElement(UPDATEQNTY).click();
//		Thread.sleep(3000);
//		driver.findElement(REMOVEPRODUCT).click();
//	}
//
//	@Then("^Product should remove from the basket$")
//	public void product_should_remove_from_the_basket() throws Throwable {
//	    Assert.assertEquals("Continue shopping", driver.findElement(By.cssSelector(".basketButtonContinue")).getText());
//	}
//
//	@Given("^User is on registration page$")
//	public void user_is_on_registration_page() throws Throwable {
//		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\chromedriver.exe");
//		driver = new ChromeDriver();
//		driver.get(baseURL);
//		driver.manage().window().maximize();
//		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());
//		
//		driver.findElement(RLOGIN).click();
//		Thread.sleep(3000);
//		driver.findElement(RREGISTER).click();
//		Thread.sleep(2000);				
//	}
//
//	@Given("^User enter all mandatory fields with valid credentials$")
//	public void user_enter_all_mandatory_fields_with_valid_credentials() throws Throwable {
//		driver.findElement(REMAIL).clear();
//		driver.findElement(REMAIL).sendKeys("varg.jame@.gmail.com");
//		Thread.sleep(2000);			
//		Select registerDropdown = new Select(driver.findElement(RTITLE));
//		registerDropdown.selectByIndex(4);
//		Thread.sleep(2000);
//		driver.findElement(RFIRSTNAME).clear();
//		driver.findElement(RFIRSTNAME).sendKeys("James");
//		Thread.sleep(2000);
//		driver.findElement(RLASTNAME).clear();
//		driver.findElement(RLASTNAME).sendKeys("Varghese");
//		Thread.sleep(2000);
//		driver.findElement(RRPASSWORD).clear();	
//		driver.findElement(RRPASSWORD).sendKeys("JVtest212");
//	}
//
//	@When("^User click on register button$")
//	public void user_click_on_register_button() throws Throwable {
//		//driver.findElement(By.cssSelector("label[for='Terms & Conditions & Privacy Policy']")).click();
//		driver.findElement(RSUBMIT).click();
//	}
//
//	@Then("^User should direct to homepage$")
//	public void user_should_direct_to_homepage() throws Throwable {
//		//Assert.assertEquals("Please check the recaptcha", driver.findElement(By.cssSelector("#reCaptcha-register")).getText());
//	}
//	@Given("^User enter invalid email along with other fields$")
//	public void user_enter_invalid_email_along_with_other_fields() throws Throwable {
//		driver.findElement(RLOGIN).click();
//		Thread.sleep(3000);
//		driver.findElement(RREGISTER).click();
//		Thread.sleep(2000);
//		//ln-c-button ln-c-button--primary regToggle
//		driver.findElement(RSUBMIT).click();
//		Thread.sleep(2000);
//		driver.findElement(REMAIL).clear();
//		driver.findElement(REMAIL).sendKeys("abc.com");
//		Thread.sleep(2000);
//		driver.findElement(RSUBMIT).click();
//		
//		Select registerDropdown = new Select(driver.findElement(RTITLE));
//		registerDropdown.selectByIndex(4);
//		Thread.sleep(2000);
//		driver.findElement(RSUBMIT).click();
//	}
//
//	@Then("^User shouldn't direct to homepage\\.$")
//	public void user_shouldn_t_direct_to_homepage() throws Throwable {
//		//Assert.assertEquals("Please check the recaptcha", driver.findElement(By.cssSelector("#reCaptcha-login")).getText());
//	}
//
//	@Given("^User click on login/register button$")
//	public void user_click_on_login_register_button() throws Throwable {
//		driver.findElement(RLOGIN).click();
//		Thread.sleep(3000);			
//	
//	}
//
//	@When("^User enter valid credentials$")
//	public void user_enter_valid_credentials() throws Throwable {
//		driver.findElement(RUSERNAME).clear();
//		driver.findElement(RUSERNAME).sendKeys("varg.james@gmail.com");
//		Thread.sleep(5000);
//		driver.findElement(RPASSWORD).clear();
//		driver.findElement(RPASSWORD).sendKeys("JVtest212");
//		
//	}
//	@When("^User enter login button$")
//	public void user_enter_login_button() throws Throwable {
//		Thread.sleep(5000);
//		driver.findElement(SUBMITLOGIN).click();
//	}
//
//	@Then("^User should see welcome message with name$")
//	public void user_should_see_welcome_message_with_name() throws Throwable {
//		Assert.assertEquals("Please check the recaptcha", driver.findElement(By.cssSelector("#reCaptcha-login")).getText());
//	}
//
//	@When("^User enter valid userid and invalid password$")
//	public void user_enter_valid_userid_and_invalid_password() throws Throwable {
//		driver.findElement(RUSERNAME).clear();
//		driver.findElement(RUSERNAME).sendKeys("varg.james@gmail.com");
//		Thread.sleep(5000);
//		driver.findElement(RPASSWORD).clear();
//		driver.findElement(RPASSWORD).sendKeys("JVtest000");		
//	}
//	@Then("^User should get error message$")
//	public void user_should_get_error_message() throws Throwable {
//		Thread.sleep(5000);
//		Assert.assertEquals("Please check the recaptcha", driver.findElement(By.cssSelector("#reCaptcha-login")).getText());
//	}
//	@When("^User enter invalid userid and valid password$")
//	public void user_enter_invalid_userid_and_valid_password() throws Throwable {
//		driver.findElement(RUSERNAME).clear();
//		driver.findElement(RUSERNAME).sendKeys("varg.com");
//		Thread.sleep(5000);
//		driver.findElement(RPASSWORD).clear();
//		driver.findElement(RPASSWORD).sendKeys("JVtest212");
//	}
//	@Given("^User select a product and product size and quantity$")
//	public void user_select_a_product_and_product_size_and_quantity() throws Throwable {
//		driver.findElement(LOUNGEWEAR).click();
//		Thread.sleep(3000);
//		//driver.findElement(By.cssSelector("p:contains('Blue Stripe & Marl Joggers 2 Pack (9 Months-6 Years)')")).click();
//		//driver.findElement(LOUNGECOLLECTIONS).click();
//		driver.findElements(LOUNGECOLLECTIONS).get(2).click();
//		Thread.sleep(3000);
//		Select sizeDropdown = new Select(driver.findElement(SIZEDROPDOWN));
//		sizeDropdown.selectByIndex(1);
//		Thread.sleep(3000);
//		Select qntyDropdown = new Select(driver.findElement(QNTYDROPDOWN));
//		qntyDropdown.selectByIndex(3);
//		Thread.sleep(5000);
//		
//	}
//
//	@Given("^User selects add to basket button$")
//	public void user_selects_add_to_basket_button() throws Throwable {
//		driver.findElement(ADDTOCART).click();
//		Thread.sleep(5000);
//		driver.findElement(ADDBASKET).click();
//		Thread.sleep(5000);
//		//driver.findElement(By.cssSelector("#rollover_cart_popup .links a")).click();	
//	}
//
//	@Given("^User selects proceed to checkout button$")
//	public void user_selects_proceed_to_checkout_button() throws Throwable {
//		driver.findElement(ADDTOCHECKOUT).click();
//		Thread.sleep(5000);
//		driver.findElement(PROCEEDTOCHECKOUT).click();		
//	}
//
//	@Given("^User select new guest and enters the new email id$")
//	public void user_select_new_guest_and_enters_the_new_email_id() throws Throwable {
//		driver.findElement(GEMAIL).clear();
//		driver.findElement(GEMAIL).sendKeys("james@gmail.com");
//		Thread.sleep(5000);
//		driver.findElement(GUESTCHECKOUT).click();
//		Thread.sleep(5000);
//		
//	}
//
//	@Given("^User clicks on guest check out button$")
//	public void user_clicks_on_guest_check_out_button() throws Throwable {
//		driver.findElement(CLICKACOLLECT).click();
//		Thread.sleep(5000);
//		driver.findElement(CANDCCONTINUE).click();
//		Thread.sleep(5000);
//		driver.findElement(CANDCADDRESS).clear();
//		driver.findElement(CANDCADDRESS).sendKeys("SL3 7BT");
//		Thread.sleep(5000);
//		driver.findElement(ADDRESSLOOKUP).click();
//		Thread.sleep(5000);
//		driver.findElements(SUBMITFORCARDDETAILS).get(0).click();
//		Thread.sleep(3000);
//		driver.findElement(GUESTSUBMIT).click();
//		Thread.sleep(5000);
//		
//	}
//
//	@Given("^User selects click & collect option and then continue$")
//	public void user_selects_click_collect_option_and_then_continue() throws Throwable {
//		driver.findElement(HDELIVERYPAYMENT).click();
//		Thread.sleep(5000);
//		driver.findElement(HCARDPAYMENT).click();
//		//Assert.assertEquals("Checkout | Tu clothing", driver.getTitle());
//		Thread.sleep(5000);		
//	}
//
//	@Given("^User enters post code and find address$")
//	public void user_enters_post_code_and_find_address() throws Throwable {
//		Select newTitle = new Select(driver.findElement(NEWTITLE));
//		newTitle.selectByIndex(2);
//		Thread.sleep(5000);
//		driver.findElement(FIRSTNAME).clear();
//		driver.findElement(FIRSTNAME).sendKeys("James");
//		Thread.sleep(5000);
//		driver.findElement(SURNAME).clear();
//		driver.findElement(SURNAME).sendKeys("Varghese");
//		Thread.sleep(5000);
//		driver.findElement(HOUSENO).clear();
//		driver.findElement(HOUSENO).sendKeys("18");
//		Thread.sleep(5000);
//		driver.findElement(POSTCODE).clear();
//		driver.findElement(POSTCODE).sendKeys("SL3 7BT");
//		Thread.sleep(5000);
//		driver.findElement(FINDADDRESS).click();
//		Thread.sleep(5000);
//	}
//
//	@Given("^User clicks on proceed to payment button$")
//	public void user_clicks_on_proceed_to_payment_button() throws Throwable {
//	
//	
//	}
//
//	@Given("^User enters the card details on the payment page$")
//	public void user_enters_the_card_details_on_the_payment_page() throws Throwable {
//		
//	}
//
//	@When("^User clicks on payment button$")
//	public void user_clicks_on_payment_button() throws Throwable {
//		driver.findElement(TERMSANDCOND).click();
//		Thread.sleep(5000);
//		//driver.findElement(CONTPAYMENT).click();
//	}
//
//	@Then("^User should get payment confirmation$")
//	public void user_should_get_payment_confirmation() throws Throwable {
//		//Assert.assertEquals("41.25",driver.findElement(By.cssSelector(".checkout-summary-total span")).getText());
//	}
//	
//	@Given("^User selects home delivery option and then continue$")
//	public void user_selects_home_delivery_option_and_then_continue() throws Throwable {
//		driver.findElement(HOMEDELIVERY).click();
//		Thread.sleep(5000);
//		driver.findElement(CLICKHOMEHOMEDELIVERY).click();
//		Thread.sleep(5000);		
//	}
//
//	@Given("^User enters the mandatory fields and find address$")
//	public void user_enters_the_mandatory_fields_and_find_address() throws Throwable {
//		Select titleDropdown = new Select(driver.findElement(HTITLE));
//		titleDropdown.selectByIndex(2);
//		Thread.sleep(3000);
//		driver.findElement(HFIRSTNAME).clear();
//		driver.findElement(HFIRSTNAME).sendKeys("James");
//		Thread.sleep(3000);
//		driver.findElement(HSURNAME).clear();
//		driver.findElement(HSURNAME).sendKeys("Varghese");
//		Thread.sleep(3000);
//		driver.findElement(HHOUSENUMBER).clear();
//		driver.findElement(HHOUSENUMBER).sendKeys("18");
//		Thread.sleep(3000);
//		driver.findElement(HPOSTCODE).clear();
//		driver.findElement(HPOSTCODE).sendKeys("SL3 7BT");
//		Thread.sleep(5000);
//		driver.findElement(HSEARCHADDRESS).click();
//		Thread.sleep(5000);
//		
//	}
//
//	@Given("^User selects the type of home delivery and proceed$")
//	public void user_selects_the_type_of_home_delivery_and_proceed() throws Throwable {
//		driver.findElement(HBILLINGADDRESS).click();
//		Thread.sleep(5000);
//		driver.findElement(HCONTINUE).click();
//		Thread.sleep(5000);
//		driver.findElement(HSTANDDELIVERY).click();
//		Thread.sleep(5000);
//		driver.findElement(HSTANDDELIVERYCONTINUE).click();
//		Thread.sleep(5000);
//		driver.findElement(HDELIVERYPAYMENT).click();
//		Thread.sleep(5000);
//		driver.findElement(HCARDPAYMENT).click();
//		Assert.assertEquals("Checkout | Tu clothing", driver.getTitle());
//		Thread.sleep(5000);
//		driver.findElement(HFINDADDRESS).click();
//		Thread.sleep(5000);
//	}
//	@Given("^User is on the store locator page$")
//	public void user_is_on_the_store_locator_page() throws Throwable {
//		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\chromedriver.exe");
//		driver = new ChromeDriver();
//		driver.get(baseURL);
//		driver.manage().window().maximize();
//		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());
//		driver.findElement(STORELOCATOR).click();
//		Thread.sleep(3000);
//		
//	}
//
//	@Given("^User enters the valid post code and click find store button$")
//	public void user_enters_the_valid_post_code_and click_find_store_button() throws Throwable {
//		driver.findElement(TEXTSTORELOCATOR).clear();
//		driver.findElement(TEXTSTORELOCATOR).sendKeys("SL3 7BT");
//		Thread.sleep(3000);
// 		driver.findElement(STORESEARCH).click();
//		
//	}
//
//	//@When("^User clicks on find stores$")
//	//public void user_clicks_on_find_stores() throws Throwable {
//	//	driver.findElement(STORESEARCH).click();
//	//}
//
//	@Then("^User should get the details of nearest stores and its opening times$")
//	public void user_should_get_the_details_of_nearest_stores_and_its_opening_times() throws Throwable {
//	    Assert.assertEquals("SL3 7BT", driver.findElement(By.cssSelector("a[class='last']")).getText());
//	}
//
//	@When("^User enters the invalid post code and click find store button$")
//	public void user_enters_the_invalid_post_code_and_click_find_store_button() throws Throwable {
//		driver.findElement(TEXTSTORELOCATOR).clear();
//		driver.findElement(TEXTSTORELOCATOR).sendKeys("SL3 TBT");
//		Thread.sleep(3000);	
//		driver.findElement(STORESEARCH).click();
//	}
//
//	@Then("^User should get an not found error message$")
//	public void user_should_get_an_not_found_error_message() throws Throwable {
//		Assert.assertEquals("Sorry, no results found for your search. Please check your spelling and make sure you are searching for a town name or postcode.", driver.findElement(By.cssSelector("p[class='ln-u-h4 ln-u-flush-bottom']")).getText());  
//	}
//	
//	@Given("^User select a size and quantity$")
//	public void user_select_a_size_and_quantity() throws Throwable {
//		Thread.sleep(3000);
//		Select sizeDropdown = new Select(driver.findElement(SIZEDROPDOWN));
//		sizeDropdown.selectByIndex(15);
//		Thread.sleep(3000);
//		Select qntyDropdown = new Select(driver.findElement(QNTYDROPDOWN));
//		qntyDropdown.selectByValue("2");
//		Thread.sleep(1000);
//	}
//
//	@When("^User click on add basket$")
//	public void user_click_on_add_basket() throws Throwable {
//		Thread.sleep(1000);
//		driver.findElement(ADDTOCART).click();
//		Thread.sleep(3000);		
//		driver.findElement(CHECKOUTBUTTON).click();
//		Thread.sleep(3000);
//		driver.findElement(CLICKANDCOLLECT).click();
//	}
//
//	@Then("^Click on click and collect button$")
//	public void click_on_click_and_collect_button() throws Throwable {
//	   //assert
//	}
//	@When("^User click on add promtioncode link$")
//	public void user_click_on_add_promtioncode_link() throws Throwable {
//		Thread.sleep(3000);
//		driver.findElement(SHOWPROMO).click();
//	}
//
////@After
////public void close() {
////	driver.close();
////}
//}
