package com.StepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.Runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class HomePageStepDef extends BaseClass{
	
	@Given("^User is on homepage$")
	public void user_is_on_homepage() throws Throwable {
		homepage.verifyHomePage();
	}
//	
//	@When("^User search for a valid product$")
//	public void user_search_for_a_valid_product() throws Throwable {
//		homepage.searchForValidProduct();
//	}
	
	@When("^User search for \"([^\"]*)\" product$")
	public void user_search_for_product(String SearchKeyword) throws Throwable {
		homepage.searchForValidProduct(SearchKeyword);
	}
	
//	@When("^User search for an invalid product$")
//	public void user_search_for_an_invalid_product() throws Throwable {
//		homepage.searchForInvalidProduct();
//	}
	
	@When("^User leave search field as blank$")
	public void user_leave_search_field_as_blank() throws Throwable {
		homepage.searchProductWithBlankField();
	}

//	@When("^User search with invalid productcode$")
//	public void user_search_with_invalid_productcode() throws Throwable {
//		homepage.searchWithInvalidProductCode();
//	}
	
	@When("^User search with \"([^\"]*)\" productcode$")
	public void user_search_with_productcode(String productCode) throws Throwable {
		homepage.searchWithInvalidProductCode(productCode);
	}
	
//	@When("^User search with valid productcode$")
//	public void user_search_with_valid_productcode() throws Throwable {
//		homepage.searchWithValidProductCode();
//	}
//	
	@When("^User search product with colour$")
	public void user_search_product_with_colour() throws Throwable {
		homepage.searchWithColour();
	}
	@When("^User search product with description$")
	public void user_search_product_with_description() throws Throwable {
		homepage.searchWithProductDescription();
		
	}
	
	@Given("^User click on login/register button$")
	public void user_click_on_login_register_button() throws Throwable {
		homepage.clickOnLoginRegister();
	
	}

}

