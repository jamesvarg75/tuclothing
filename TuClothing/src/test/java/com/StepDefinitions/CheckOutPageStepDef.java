package com.StepDefinitions;

import com.Runner.BaseClass;

import cucumber.api.java.en.Given;

public class CheckOutPageStepDef extends BaseClass{
	
	@Given("^User selects proceed to checkout button$")
	public void user_selects_proceed_to_checkout_button() throws Throwable {
		chekOutPage.verifyProceedToCheckout();		
	}

	@Given("^User select new guest and enters the new email id$")
	public void user_select_new_guest_and_enters_the_new_email_id() throws Throwable {
		chekOutPage.verifyGuestDetails();
	}

	@Given("^User clicks on guest check out button$")
	public void user_clicks_on_guest_check_out_button() throws Throwable {
		chekOutPage.verifyGuestCheckOutButton();		
	}

}

