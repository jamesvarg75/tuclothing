package com.StepDefinitions;

import com.Runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class BasketPageStepDef extends BaseClass{

	@When("^User select size and multiple quantity and click on to addbasket$")
	public void user_select_size_and_multiple_quantity_and_click_on_to_addbasket() throws Throwable {
		basketPage.verifySelectionAndAddToBasket();
	}
	
	@Given("^User select size and quantity and click on to addbasket$")
	public void user_select_size_and_quantity_and_click_on_to_addbasket() throws Throwable {
		basketPage.verifySelectionAndUpdateToBasket();
	}
	
	@When("^User select remove product option$")
	public void user_select_remove_product_option() throws Throwable {
		basketPage.verifyRemoveFromBasket();
	}
	
	@Given("^User selects add to basket button$")
	public void user_selects_add_to_basket_button() throws Throwable {
		basketPage.verifyAddBasketForCheckOut();
	}
	
	@Given("^User select a size and quantity$")
	public void user_select_a_size_and_quantity() throws Throwable {
		basketPage.verifyClickAndCollectProducts();
	}
	
	@When("^User click on add basket$")
	public void user_click_on_add_basket() throws Throwable {
		basketPage.verifyClickOnAddBasket();
	}
	
	@When("^User click on add promtioncode link$")
	public void user_click_on_add_promtioncode_link() throws Throwable {
		basketPage.verifyPromLink();
	}
}
