package com.StepDefinitions;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.Pages.StoreLocatorPage;
import com.Runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StoreLocatorPageStepDef extends BaseClass {
	
	@Given("^User is on the store locator page$")
	public void user_is_on_the_store_locator_page() throws Throwable {
		storeLocatorPage.searchStoreLocation();		
	}
	
	@When("^User enters the valid post code and click find store button$")
	public void user_enters_the_valid_post_code_and_click_find_store_button() throws Throwable {
		storeLocatorPage.verifyVaildPostcode();	
		storeLocatorPage.storeSearch();
	}
	
	@When("^User enters the invalid post code and click find store button$")
	public void user_enters_the_invalid_post_code_and_click_find_store_button() throws Throwable {
		storeLocatorPage.verifyInvalidPostcode();
		storeLocatorPage.storeSearch();		
	}
	
	
}
