package com.StepDefinitions;

import com.Runner.BaseClass;

import cucumber.api.java.en.Given;

public class BillingPageStepDef extends BaseClass{
	
	@Given("^User enters post code and find address$")
	public void user_enters_post_code_and_find_address() throws Throwable {
		billingPage.verifyBillingPaymentDetails();
	}

}
