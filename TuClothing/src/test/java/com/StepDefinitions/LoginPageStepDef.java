package com.StepDefinitions;

import com.Runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

public class LoginPageStepDef extends BaseClass{
	
	@Given("^User is on registration page$")
	public void user_is_on_registration_page() throws Throwable {
		loginPage.verifyLoginPage();
	}

	@Given("^User enter all mandatory fields with valid credentials$")
	public void user_enter_all_mandatory_fields_with_valid_credentials() throws Throwable {
		loginPage.verifyValidCredentials();
	}
	
	@When("^User click on register button$")
	public void user_click_on_register_button() throws Throwable {
		loginPage.verifySubmit();		
	}
	
	@Given("^User enter invalid email along with other fields$")
	public void user_enter_invalid_email_along_with_other_fields() throws Throwable {
		loginPage.verifyWithInvalidEmail();
	}
	
	
}
