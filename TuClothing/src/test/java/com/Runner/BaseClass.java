package com.Runner;

import org.openqa.selenium.WebDriver;

import com.Driver.Action;
import com.Driver.Get;
import com.Pages.BasketPage;
import com.Pages.BillingPage;
import com.Pages.ChekOutPage;
import com.Pages.DeliveryPage;
import com.Pages.HomePage;
import com.Pages.LoginPage;
import com.Pages.PaymentPage;
import com.Pages.ProductDetailsPage;
import com.Pages.RegistrationPage;
import com.Pages.SearchResultsPage;
import com.Pages.StoreLocatorPage;

public class BaseClass {
	public static WebDriver driver;
	public static HomePage homepage = new HomePage();
	public static SearchResultsPage searchResultsPage = new SearchResultsPage();
	
	//Storelocation Funcationality
	public static StoreLocatorPage storeLocatorPage = new StoreLocatorPage();
	
	public static ProductDetailsPage productDetailsPage = new ProductDetailsPage();
	
	public static LoginPage loginPage = new LoginPage();
	
	public static RegistrationPage registrationPage= new RegistrationPage();
	
	public static BasketPage basketPage = new BasketPage();
	
	public static ChekOutPage chekOutPage = new ChekOutPage();
	
	public static PaymentPage paymentPage = new PaymentPage();
	
	public static BillingPage billingPage = new BillingPage();
	
	public static DeliveryPage deliveryPage = new DeliveryPage();
	
	public static Action action = new Action(); 
	public static Get get= new Get();
	
}
