package com.Driver;

import org.openqa.selenium.By;

import com.Runner.BaseClass;

public class Action extends BaseClass {
	
	public void clickOnTheElement(By elementName) {
		driver.findElement(elementName).click();
	}
	
	public void clickOnElements(By elementName, int indexvalue) {
		driver.findElements(elementName).get(indexvalue).click();
	}
	
	public void sendKeysElement(By elementName, String keyword) {
		driver.findElement(elementName).clear();
		driver.findElement(elementName).sendKeys(keyword);
	}

}
