package com.Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.Runner.BaseClass;

public class LoginPage extends BaseClass{
	
	private static By RLOGIN= By.linkText("Tu Log In / Register");
	private static By RREGISTER = By.cssSelector(".regToggle");
	private static By REMAIL= By.cssSelector("#register_email");
	private static By RTITLE = By.cssSelector("#register_title");
	private static By RFIRSTNAME = By.cssSelector("#register_firstName");
	private static By RLASTNAME= By.cssSelector("#register_lastName");
	private static By RRPASSWORD= By.cssSelector("#password");
	private static By RSUBMIT= By.cssSelector("#submit-register");
	
	public void verifyLoginPage() throws InterruptedException {
		homepage.verifyHomePage();
		
		action.clickOnTheElement(RLOGIN);
		Thread.sleep(3000);
		action.clickOnTheElement(RREGISTER);
		Thread.sleep(2000);	
	}
	
	public void verifyValidCredentials() throws InterruptedException {
		action.sendKeysElement(REMAIL, "varg.jame@.gmail.com");
		Thread.sleep(2000);			
		Select registerDropdown = new Select(driver.findElement(RTITLE));
		registerDropdown.selectByIndex(4);
		Thread.sleep(2000);
		action.sendKeysElement(RFIRSTNAME, "James");
		Thread.sleep(2000);
		action.sendKeysElement(RLASTNAME, "Varghese");
		Thread.sleep(2000);
		action.sendKeysElement(RRPASSWORD, "JVtest212");
	}

	public void verifySubmit() {
		action.clickOnTheElement(RSUBMIT);
	}
	
	public void verifyWithInvalidEmail() throws InterruptedException {
		action.clickOnTheElement(RLOGIN);
		Thread.sleep(3000);
		action.clickOnTheElement(RREGISTER);
		Thread.sleep(2000);	
		action.clickOnTheElement(RSUBMIT);
		Thread.sleep(2000);
		action.sendKeysElement(REMAIL, "abc.com");
		Thread.sleep(2000);
		action.clickOnTheElement(RSUBMIT);
		
		Select registerDropdown = new Select(driver.findElement(RTITLE));
		registerDropdown.selectByIndex(4);
		Thread.sleep(2000);
		action.clickOnTheElement(RSUBMIT);
	}

}
