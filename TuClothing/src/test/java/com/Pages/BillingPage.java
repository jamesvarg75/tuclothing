package com.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import com.Runner.BaseClass;

public class BillingPage extends BaseClass{
	
	private static By NEWTITLE = By.cssSelector("#newTitle");
	private static By FIRSTNAME = By.cssSelector("input[data-testid='firstName']");
	private static By SURNAME = By.cssSelector("input[data-testid='surname']");
	private static By HOUSENO = By.cssSelector("input[data-testid='houseNumber']");
	private static By POSTCODE = By.cssSelector("input[data-testid='postCode']");
	private static By FINDADDRESS = By.cssSelector("button[data-testid='findAddress']");
	
	public void verifyBillingPaymentDetails() throws InterruptedException {
		Select newTitle = new Select(driver.findElement(NEWTITLE));
		newTitle.selectByIndex(2);
		Thread.sleep(5000);
		action.sendKeysElement(FIRSTNAME, "James");
		Thread.sleep(5000);
		action.sendKeysElement(SURNAME, "Varghese");
		Thread.sleep(5000);
		action.sendKeysElement(HOUSENO, "18");
		Thread.sleep(5000);
		action.sendKeysElement(POSTCODE, "SL3 7BT");
		Thread.sleep(5000);
		action.clickOnTheElement(FINDADDRESS);
		Thread.sleep(5000);
	}

}
