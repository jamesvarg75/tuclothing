package com.Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.Runner.BaseClass;

public class HomePage extends BaseClass {
//	public static WebDriver driver;
//	private static String baseURL = "https://tuclothing.sainsburys.co.uk/";
	private static By TEXTSEARCH = By.cssSelector("#search");
	private static By TEXTSEARCHBUTTON= By.cssSelector(".searchButton");
	private static By LOGINORREGISTER = By.linkText("Tu Log In / Register");
	
	public void verifyHomePage() {	
//		System.setProperty("webdriver.chrome.driver", "C:\\Automation\\chromedriver.exe");
//		driver = new ChromeDriver();
//		driver.get(baseURL);
//		driver.manage().window().maximize();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", driver.getCurrentUrl());
	}
	public void searchForValidProduct(String searchKeyword) throws InterruptedException {
		action.sendKeysElement(TEXTSEARCH, searchKeyword);
		Thread.sleep(3000);
		action.clickOnTheElement(TEXTSEARCHBUTTON);
	}
//	public void searchForInvalidProduct() throws InterruptedException {
//		action.sendKeysElement(TEXTSEARCH, "Liquor");
//		Thread.sleep(3000);
//		action.clickOnTheElement(TEXTSEARCHBUTTON);		
//	}
	public void searchProductWithBlankField() throws InterruptedException {
		action.sendKeysElement(TEXTSEARCH, "");
		Thread.sleep(3000);
		action.clickOnTheElement(TEXTSEARCHBUTTON);		
	}
	public void searchWithInvalidProductCode(String productCode) throws InterruptedException {
		action.sendKeysElement(TEXTSEARCH, productCode);
		Thread.sleep(3000);
		action.clickOnTheElement(TEXTSEARCHBUTTON);
	}
//	public void searchWithValidProductCode() throws InterruptedException {
//		action.sendKeysElement(TEXTSEARCH, "137431127");
//		Thread.sleep(3000);
//		action.clickOnTheElement(TEXTSEARCHBUTTON);		
//	}
	public void searchWithColour() throws InterruptedException {
		action.sendKeysElement(TEXTSEARCH, "Black");
		Thread.sleep(3000);
		action.clickOnTheElement(TEXTSEARCHBUTTON);		
	}
	public void searchWithProductDescription() throws InterruptedException {
		action.sendKeysElement(TEXTSEARCH, "Yellow Oversized Knitted Top");
		Thread.sleep(3000);
		action.clickOnTheElement(TEXTSEARCHBUTTON);		
	}
	
	//Login feature
	
	public void clickOnLoginRegister() throws InterruptedException {
		action.clickOnTheElement(LOGINORREGISTER);
		Thread.sleep(3000);		
	}
	
}


