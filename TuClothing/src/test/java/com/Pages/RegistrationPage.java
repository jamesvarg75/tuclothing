package com.Pages;

import org.openqa.selenium.By;

import com.Runner.BaseClass;

public class RegistrationPage extends BaseClass{
	
	private static By RUSERNAME = By.cssSelector("#j_username");
	private static By RPASSWORD = By.cssSelector("#j_password");
	private static By SUBMITLOGIN = By.cssSelector("#submit-login");

	public void verifyValidLoginCredentials(String email,String password) throws InterruptedException {
		action.sendKeysElement(RUSERNAME, email);
		Thread.sleep(5000);
		action.sendKeysElement(RPASSWORD, password);
		
	}
	
	public void verifyLoginButton() throws InterruptedException {
		Thread.sleep(5000);
		action.clickOnTheElement(SUBMITLOGIN);
	}
	
	public void verifyLoginWithInvalidPassword() throws InterruptedException {
		action.sendKeysElement(RUSERNAME, "varg.james@gmail.com");
		Thread.sleep(5000);
		action.sendKeysElement(RPASSWORD, "JVtest000");
	}
	
	public void verifyLoginWithInvalidUserid() throws InterruptedException {
		action.sendKeysElement(RUSERNAME, "varg.com");
		Thread.sleep(5000);
		action.sendKeysElement(RPASSWORD, "JVtest212");
	}
	

	
}
