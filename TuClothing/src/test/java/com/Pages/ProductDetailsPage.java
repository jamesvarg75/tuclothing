package com.Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.Runner.BaseClass;

public class ProductDetailsPage extends BaseClass {
	
	private static By MENCATEGORYBUTTON = By.linkText("Men");
	private static By MENCATEGORYSUBBUTTON = By.linkText("Jeans");
	private static By MENPRODUCTLIST = By.cssSelector(".image-component img");
	private static By LOUNGEWEAR = By.linkText("Loungewear");
	private static By LOUNGECOLLECTIONS = By.cssSelector(".image-component img");
	private static By SIZEDROPDOWN = By.cssSelector("#select-size");
	private static By QNTYDROPDOWN = By.cssSelector("#productVariantQty");


	public void verifyProductDeailsPage() throws InterruptedException {
		homepage.verifyHomePage();
		action.clickOnTheElement(MENCATEGORYBUTTON);
		//Assert.assertEquals("Menswear", driver.findElement(By.cssSelector("h2:contains('Menswear')")).getText());
		//Assert.assertEquals("Mens Clothing | Jeans, T-Shirts, Shirts | Tu Clothing", driver.getTitle());
		Thread.sleep(2000);
		action.clickOnTheElement(MENCATEGORYSUBBUTTON);
		Thread.sleep(2000);
		//driver.findElements(MENPRODUCTLIST).get(0).click();
		action.clickOnElements(MENPRODUCTLIST, 0);
//		Thread.sleep(3000);
	}
	
	public void verifyCheckoutProducts() throws InterruptedException {
		action.clickOnTheElement(LOUNGEWEAR);
		Thread.sleep(3000);
		//driver.findElement(By.cssSelector("p:contains('Blue Stripe & Marl Joggers 2 Pack (9 Months-6 Years)')")).click();
		//driver.findElement(LOUNGECOLLECTIONS).click();
		action.clickOnElements(LOUNGECOLLECTIONS, 1);
		Thread.sleep(3000);
		Select sizeDropdown = new Select(driver.findElement(SIZEDROPDOWN));
		sizeDropdown.selectByIndex(1);
		Thread.sleep(3000);
		Select qntyDropdown = new Select(driver.findElement(QNTYDROPDOWN));
		qntyDropdown.selectByIndex(2);
		Thread.sleep(5000);
	}
}