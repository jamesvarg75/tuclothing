package com.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import com.Runner.BaseClass;

public class BasketPage extends BaseClass{
	private static By SIZEDROPDOWN_TESTING = By.cssSelector("#select-size");
	private static By SIZEDROPDOWN = By.cssSelector("#select-size");
	private static By QNTYDROPDOWN = By.cssSelector("#productVariantQty");
	private static By ADDTOCART = By.cssSelector("#AddToCart");
	private static By CARTBUTTON = By.cssSelector("#cart_button");
	private static By NEWQNTYDROPDOWN = By.cssSelector("#quantity0");
	private static By UPDATEQNTY= By.cssSelector("#QuantityProduct_0");
	private static By REMOVEPRODUCT= By.cssSelector("#RemoveProduct_0");
	private static By ADDBASKET = By.cssSelector("#basket-title");
	private static By CHECKOUTBUTTON = By.cssSelector(".doCheckoutBut.tuButton.ln-c-button.ln-c-button--primary");
	private static By SHOWPROMO = By.cssSelector("#showPromo span");
	private static By CLICKANDCOLLECT = By.linkText("Click & Collect");
	
	public void verifySelectionAndAddToBasket() throws InterruptedException {
		Thread.sleep(5000);
		Select sizeDropdown = new Select(driver.findElement(SIZEDROPDOWN));
		//Select sizeDropdown = new Select(action.clickOnTheElement(SIZEDROPDOWN));
		sizeDropdown.selectByIndex(15);
		Thread.sleep(3000);
		Select qntyDropdown = new Select(driver.findElement(QNTYDROPDOWN));
		qntyDropdown.selectByValue("1");
		Thread.sleep(3000);
		action.clickOnTheElement(ADDTOCART);
		Thread.sleep(3000);
		qntyDropdown.selectByValue("2");
		Thread.sleep(3000);
		action.clickOnTheElement(ADDTOCART); // no actions will happen, you have to do one one addtocart click.
		Thread.sleep(3000);
		action.clickOnTheElement(ADDTOCART); // Error message will appear now.
	}
	
	public void verifySelectionAndUpdateToBasket() throws InterruptedException {
		Select sizeDropdown = new Select(driver.findElement(SIZEDROPDOWN));
		sizeDropdown.selectByIndex(15);
		Thread.sleep(3000);
		Select qntyDropdown = new Select(driver.findElement(QNTYDROPDOWN));
		qntyDropdown.selectByValue("2");
		Thread.sleep(3000);				
		action.clickOnTheElement(ADDTOCART);
		Thread.sleep(3000);//			
		action.clickOnTheElement(CARTBUTTON);
		Thread.sleep(3000);//
		Select newQntyDropdown = new Select(driver.findElement(NEWQNTYDROPDOWN));
		newQntyDropdown.selectByValue("10");
		Thread.sleep(3000);
	}
	
	public void verifyRemoveFromBasket() throws InterruptedException {
		action.clickOnTheElement(UPDATEQNTY);
		Thread.sleep(5000);
		action.clickOnTheElement(REMOVEPRODUCT);
		Thread.sleep(5000);
	}
	
	public void verifyAddBasketForCheckOut() throws InterruptedException {
		action.clickOnTheElement(ADDTOCART);
		Thread.sleep(5000);
		action.clickOnTheElement(ADDBASKET);
		Thread.sleep(5000);
	}
	
	public void verifyClickAndCollectProducts() throws InterruptedException {
		Thread.sleep(3000);
		Select sizeDropdown = new Select(driver.findElement(SIZEDROPDOWN));
		sizeDropdown.selectByIndex(15);
		Thread.sleep(3000);
		Select qntyDropdown = new Select(driver.findElement(QNTYDROPDOWN));
		qntyDropdown.selectByValue("2");
		Thread.sleep(1000);
	}
	
	public void verifyClickOnAddBasket() throws InterruptedException {
		Thread.sleep(1000);
		action.clickOnTheElement(ADDTOCART);
		Thread.sleep(3000);		
		action.clickOnTheElement(CHECKOUTBUTTON);
		Thread.sleep(5000);
	//	action.clickOnTheElement(CLICKANDCOLLECT);
	}
	
	public void verifyPromLink() throws InterruptedException {
		//Thread.sleep(3000);
		action.clickOnTheElement(SHOWPROMO);
		Thread.sleep(5000);
	}
}
