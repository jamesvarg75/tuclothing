package com.Pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.Runner.BaseClass;

public class PaymentPage extends BaseClass{

	private static By HDELIVERYPAYMENT = By.cssSelector("button[data-testid='continueToPayment']");
	private static By HCARDPAYMENT = By.linkText("Pay with a card");
	private static By TERMSANDCOND = By.cssSelector("label[data-testid='termsAndConditions']");
	private static By CONTPAYMENT = By.cssSelector("button[id='contPayment']");
	private static By HBILLINGADDRESS= By.cssSelector("label[for='useDeliveryAddressForBillingAddress']");
	private static By HCONTINUE= By.cssSelector("#continue");
	private static By HSTANDDELIVERY= By.cssSelector("label[for='standard-delivery']");
	private static By HSTANDDELIVERYCONTINUE = By.cssSelector("input[value='Continue']");
	private static By HFINDADDRESS= By.cssSelector("button[data-testid='findAddress']");
	
	public void verifyClickAndCollectPayment() throws InterruptedException {
		action.clickOnTheElement(HDELIVERYPAYMENT);
		Thread.sleep(5000);
		action.clickOnTheElement(HCARDPAYMENT);
		//Assert.assertEquals("Checkout | Tu clothing", driver.getTitle());
		Thread.sleep(5000);	
	}
	
	public void verifyPayment() throws InterruptedException {
		action.clickOnTheElement(TERMSANDCOND);
		Thread.sleep(5000);
		//driver.findElement(CONTPAYMENT).click();
	}
	public void verifyProceedPayment() {
		
	}
	
	public void verifyUserCardDetails() {
		
	}
	
	public void verifyHomeDeliveryPaymentDetails() throws InterruptedException {
		action.clickOnTheElement(HBILLINGADDRESS);
		Thread.sleep(5000);
		action.clickOnTheElement(HCONTINUE);
		Thread.sleep(5000);
		action.clickOnTheElement(HSTANDDELIVERY);
		Thread.sleep(5000);
		action.clickOnTheElement(HSTANDDELIVERYCONTINUE);
		Thread.sleep(5000);
		action.clickOnTheElement(HDELIVERYPAYMENT);
		Thread.sleep(5000);
		action.clickOnTheElement(HCARDPAYMENT);
		//Assert.assertEquals("Checkout | Tu clothing", driver.getTitle());
		Thread.sleep(5000);
		action.clickOnTheElement(HFINDADDRESS);
		Thread.sleep(5000);
	}
}
