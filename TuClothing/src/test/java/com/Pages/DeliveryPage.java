package com.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import com.Runner.BaseClass;

public class DeliveryPage extends BaseClass{
	
	private static By HOMEDELIVERY = By.cssSelector("label[for='HOME_DELIVERY']");
	private static By CLICKHOMEHOMEDELIVERY= By.cssSelector("input[data-testid='continue']");
	private static By HTITLE= By.cssSelector("select[id='address.title']");
	private static By HFIRSTNAME= By.cssSelector("input[id='address.firstName']");
	private static By HSURNAME= By.cssSelector("input[id='address.surname']");
	private static By HHOUSENUMBER= By.cssSelector("input[id='addressDeliveryhouseNameOrNumber']");
	private static By HPOSTCODE= By.cssSelector("input[id='addressPostcode']");
	private static By HSEARCHADDRESS= By.cssSelector("button[class='ln-c-button ln-c-button--primary address-lookup ln-u-push-bottom']");
	
	public void verifyHomeDeliveryDetails() throws InterruptedException {
		action.clickOnTheElement(HOMEDELIVERY);
		Thread.sleep(5000);
		action.clickOnTheElement(CLICKHOMEHOMEDELIVERY);
		Thread.sleep(5000);	
	}
	
	public void verifyDeliveryAddress() throws InterruptedException {
		Select titleDropdown = new Select(driver.findElement(HTITLE));
		titleDropdown.selectByIndex(2);
		Thread.sleep(3000);
		action.sendKeysElement(HFIRSTNAME, "James");
		Thread.sleep(3000);
		action.sendKeysElement(HSURNAME, "Varghese");
		Thread.sleep(3000);
		action.sendKeysElement(HHOUSENUMBER, "18");
		Thread.sleep(3000);
		action.sendKeysElement(HPOSTCODE, "SL3 7BT");
		Thread.sleep(5000);
		action.clickOnTheElement(HSEARCHADDRESS);
		Thread.sleep(5000);
	}

}
