package com.Pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import com.Runner.BaseClass;

public class StoreLocatorPage extends BaseClass {
	
	private static By STORELOCATOR = By.linkText("Tu Store Locator");
	private static By TEXTSTORELOCATOR= By.cssSelector(".ln-c-text-input.ln-u-push-bottom");
	private static By STORESEARCH= By.cssSelector("button[class='ln-c-button ln-c-button--primary']");
	
	public void searchStoreLocation() throws InterruptedException {
		homepage.verifyHomePage();
		action.clickOnTheElement(STORELOCATOR);
		Thread.sleep(3000);
	}
	
	public void verifyVaildPostcode() throws InterruptedException {
		action.sendKeysElement(TEXTSTORELOCATOR, "SL3 7BT");
		Thread.sleep(3000);
	}
	
	public void storeSearch() {
		action.clickOnTheElement(STORESEARCH);
	}

	public void verifyInvalidPostcode() throws InterruptedException {
		action.sendKeysElement(TEXTSTORELOCATOR, "SL3 TBT");
		Thread.sleep(3000);	
	}
}
