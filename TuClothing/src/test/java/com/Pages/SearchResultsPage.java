package com.Pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.Runner.BaseClass;

public class SearchResultsPage extends BaseClass {
	private static By SEARCHERROR  = By.cssSelector("h1");
	private static By PRODUCTSEARCH = By.cssSelector("#search-empty-errors");

	public void verifySearchResultsPage() {
		Assert.assertEquals("Search results for: Jeans | Tu clothing", driver.getTitle());
	}
	
	public void verifySearchResultError() {
		Assert.assertEquals("Sorry, no results for 'Liquor'", get.getTextElement(SEARCHERROR));
	} //driver.findElement(SEARCHERROR).getText());
	
	public void completeProductSearch() {
		Assert.assertEquals("Please complete a product search", get.getTextElement(PRODUCTSEARCH));
	} //driver.findElement(PRODUCTSEARCH).getText());
	
	public void verifyInvalidProductCode () {
		Assert.assertEquals("Search results for: 433545335 | Tu clothing", driver.getTitle());
	}
	
	public void verifyValidProductCode() {
		Assert.assertEquals("Search results for: 137431127 | Tu clothing", driver.getTitle());
	}
	
	public void verifyProductWithColour() {
		Assert.assertTrue(driver.findElement(By.cssSelector("h1")).getText().toLowerCase().contains("black"));
	}
	
	public void verifyRelatedProducts() {
		Assert.assertTrue(driver.findElement(By.cssSelector("h1")).getText().toLowerCase().contains("yellow oversized knitted top"));
	}
	
	public void verifyPostcode() {
		 Assert.assertEquals("SL3 7BT", driver.findElement(By.cssSelector("a[class='last']")).getText());
	}
	public void verifyInvalidPostcode() {
		Assert.assertEquals("Sorry, no results found for your search. Please check your spelling and make sure you are searching for a town name or postcode.", driver.findElement(By.cssSelector("p[class='ln-u-h4 ln-u-flush-bottom']")).getText());
	}
	
	public void verifyProductDetailResults() {
		Assert.assertEquals("Qty:2", driver.findElement(By.cssSelector(".prod_quantity")).getText());
	}
	
	public void verifyRemovalProductResult() {
		 Assert.assertEquals("Continue shopping", driver.findElement(By.cssSelector(".basketButtonContinue")).getText());
	}
	
	public void verifyValidLoginResults() {
		//Assert.assertEquals("Please check the recaptcha", driver.findElement(By.cssSelector("#reCaptcha-register")).getText());
	}
	
	public void verifyInvalidLoginResults() {
		//Assert.assertEquals("Please check the recaptcha", driver.findElement(By.cssSelector("#reCaptcha-login")).getText());
	}
	
	public void verifyLoginHomePage() {
		Assert.assertEquals("Please check the recaptcha", driver.findElement(By.cssSelector("#reCaptcha-login")).getText());
	}
	
	public void verifyLoginErrorMessage() {
		Assert.assertEquals("Please check the recaptcha", driver.findElement(By.cssSelector("#reCaptcha-login")).getText());
	}
	
	public void verifyPaymentConfirmation() {
		//Assert.assertEquals("41.25",driver.findElement(By.cssSelector(".checkout-summary-total span")).getText());
	}
	
	public void verifyClickAndCollectResults() {
		
	}
}

