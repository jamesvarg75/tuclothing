package com.Pages;

import org.openqa.selenium.By;

import com.Runner.BaseClass;

public class ChekOutPage extends BaseClass{
	
	private static By ADDTOCHECKOUT= By.cssSelector("a[class='ln-c-button ln-c-button--primary']");
	private static By PROCEEDTOCHECKOUT = By.cssSelector("a[class='ln-c-button ln-c-button--primary ln-u-soft-sides*1/4 tuButton']");
	private static By GEMAIL= By.cssSelector("#guest_email");
	private static By GUESTCHECKOUT = By.cssSelector("button[data-testid='guest_checkout']");
	private static By CLICKACOLLECT = By.cssSelector("label[for='CLICK_AND_COLLECT']");
	private static By CANDCCONTINUE = By.cssSelector("input[data-testid='continue']");
	private static By CANDCADDRESS = By.cssSelector("#lookup");
	private static By ADDRESSLOOKUP = By.cssSelector("button[data-testid='lookup-submit']");
	private static By SUBMITFORCARDDETAILS = By.cssSelector("button[class='StoreCard-module__selectStoreButton--3CFno']");
	private static By GUESTSUBMIT= By.cssSelector("button[data-testid='submit-button']");
	
	public void verifyProceedToCheckout() throws InterruptedException {
		action.clickOnTheElement(ADDTOCHECKOUT);
		Thread.sleep(5000);
		action.clickOnTheElement(PROCEEDTOCHECKOUT);
	}
	
	public void verifyGuestDetails() throws InterruptedException {
		action.sendKeysElement(GEMAIL, "james@gmail.com");
		Thread.sleep(5000);
		action.clickOnTheElement(GUESTCHECKOUT);
		Thread.sleep(5000);
	}
	
	public void verifyGuestCheckOutButton() throws InterruptedException {
		action.clickOnTheElement(CLICKACOLLECT);
		Thread.sleep(5000);
		action.clickOnTheElement(CANDCCONTINUE);
		Thread.sleep(5000);
		action.sendKeysElement(CANDCADDRESS, "SL3 7BT");
		Thread.sleep(5000);
		action.clickOnTheElement(ADDRESSLOOKUP);
		Thread.sleep(5000);
		action.clickOnElements(SUBMITFORCARDDETAILS, 0);
		Thread.sleep(3000);
		action.clickOnTheElement(GUESTSUBMIT);
		Thread.sleep(5000);
	}
	

}
