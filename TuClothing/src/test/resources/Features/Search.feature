Feature: Search Functionality

Scenario: Verify search with valid product name

Given User is on homepage
When User search for "Jeans" product
Then User should see the search product.

Scenario: Verify search with invalid product

Given User is on homepage
When User search for "Liquor" product
Then User should see an error message

Scenario: Verify search with blank a field

Given User is on homepage
When User leave search field as blank
Then User should be prompt to fill the search field

Scenario: Verify search with invaild productcode

Given User is on homepage
When User search with "433545335" productcode
Then User should get an error message

Scenario: Veriy product with valid productcode

Given User is on homepage
When User search with "137431127" productcode
Then User should see search products 

Scenario: Verify search with product colour

Given User is on homepage
When User search product with colour
Then User should see the selected colour with products


Scenario: verify search with product description#

Given User is on homepage
When User search product with description
Then User should see related products
