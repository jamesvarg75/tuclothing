Feature: Login Functionality

Scenario: Verify login with valid credentials

Given User is on homepage
And User click on login/register button
When User enter valid credentials
|email|varg.james@gmail.com|
|password|JVtest212|
And User enter login button
Then User should see welcome message with name

Scenario: Verify login with valid userID and invalid password

Given User is on homepage
And User click on login/register button
When User enter valid userid and invalid password
And User enter login button
Then User should get error message

Scenario: Verify login with invalid userid and valid password

Given User is on homepage
And User click on login/register button
When User enter invalid userid and valid password
And User enter login button
Then User should get error message
 