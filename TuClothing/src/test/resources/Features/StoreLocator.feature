Feature: Store Locator Functionality

Scenario: Verify the functionality with valid postcode

Given User is on the store locator page
When User enters the valid post code and click find store button
Then User should get the details of nearest stores and its opening times

Scenario: Verify the functionality with invalid postcode

Given User is on the store locator page
When User enters the invalid post code and click find store button
Then User should get an not found error message 
