Feature: Register Functionality

Scenario: Verify registration functionality by providing all valid credentials

Given User is on registration page
And User enter all mandatory fields with valid credentials
When User click on register button
Then User should direct to homepage

Scenario: Verify registration with invalid email

Given User is on registration page
And User enter invalid email along with other fields
When User click on register button
Then User shouldn't direct to homepage.