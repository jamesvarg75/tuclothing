Feature: Check out functionality

Scenario: Verify the functionality when the guest place an order by click & collect option

Given User is on homepage
And User select a product and product size and quantity
And User selects add to basket button
And User selects proceed to checkout button
And User select new guest and enters the new email id
And User clicks on guest check out button
And User selects click & collect option and then continue
And User enters post code and find address
And User clicks on proceed to payment button
And User enters the card details on the payment page
When User clicks on payment button
Then User should get payment confirmation

Scenario: Verify the functionality when the guest place an order by home delivery option

Given User is on homepage
And User select a product and product size and quantity
And User selects add to basket button
And User selects proceed to checkout button
And User select new guest and enters the new email id
And User selects home delivery option and then continue
And User enters the mandatory fields and find address
And User selects the type of home delivery and proceed
And User clicks on proceed to payment button
And User enters the card details on the payment page
And User clicks on payment button
Then User should get payment confirmation