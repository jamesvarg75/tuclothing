Feature: Basket Functionality

Scenario: Verify the functionality by adding multiple quantity to the basket

Given User is on product details page
When User select size and multiple quantity and click on to addbasket
Then Product should add to basket

Scenario: Verify the functionality by removing a product from the basket

Given User is on product details page
And User select size and quantity and click on to addbasket
When User select remove product option
Then Product should remove from the basket

Scenario: Verify the functionality by click and collect

Given User is on product details page
And User select a size and quantity
When User click on add basket
Then Click on click and collect button

Scenario: Verify the functionality promotioncode

Given User is on product details page
And User select a size and quantity
When User click on add basket
And User click on add promtioncode link
Then Click on click and collect button